package com.aire.utils;

import java.io.IOException;
import java.util.Properties;

public class DatabaseProperties {
	private DatabaseProperties(){}
		
	public static String get(String property){
		Properties prop = new Properties();

		try {
			prop.load(DatabaseProperties.class.getResourceAsStream("/database.properties"));
			return prop.getProperty(property);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
