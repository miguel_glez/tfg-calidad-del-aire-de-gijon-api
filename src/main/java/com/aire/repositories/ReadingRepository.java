package com.aire.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aire.domain.Reading;
import com.aire.domain.Station;

@Repository
public interface ReadingRepository extends CrudRepository<Reading, Long> {
	List<Reading> findTop168ByStationOrderByReadingIDDesc(Station station);
	Reading findFirstByStationOrderByReadingIDDesc(Station station);
}