package com.aire.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.aire.repositories.ReadingRepository;

public class ReadingRepositoryTest extends AireTest {
	
	@Autowired
	private ReadingRepository repository;
	
	// RR-001
	@Test
	public void whenFindTop168ByStationOrderByReadingIDDesc_thenReturnReadings(){
		// tenemos
		Station castilla = insertStation(insertCity());
		insertReadings(castilla, 200);
		
		// probamos
		List<Reading> readings = repository.findTop168ByStationOrderByReadingIDDesc(castilla);
		
		// comprobamos
		assertThat( readings.size() ).isEqualTo(168); 
		assertThat( readings.get(0).getDate().getHour()).isEqualTo( LocalDateTime.now().getHour() );
	}
	
	// RR-002
	@Test
	public void whenFindFirstByStationOrderByReadingIDDesc_thenReturnReading(){
		// tenemos
		Station castilla = insertStation(insertCity());
		insertReadings(castilla, 200);
			
		// probamos
		Reading reading = repository.findFirstByStationOrderByReadingIDDesc(castilla);
		
		// comprobamos
		assertThat( reading.getDate().getHour() ).isEqualTo( LocalDateTime.now().getHour() );
	}
}
