package com.aire.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aire.domain.City;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {
	City findByName(String name);
}
