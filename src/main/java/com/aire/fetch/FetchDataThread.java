package com.aire.fetch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

import com.aire.domain.Station;
import com.aire.services.CityService;
import com.aire.services.StationService;
import com.aire.utils.Keys;

@Component //(value="fetchDataThread")
@Scope("singleton")
public class FetchDataThread extends Thread{
	@Autowired
	StationService sService;
	@Autowired
	CityService cService;
	
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private ApplicationContext applicationContext;
	
	private StationReader reader;
	
	@Override
	public void run(){
		while (true){
			try {
				reader = new StationReader(new RestTemplateBuilder());
				
				// comprobamos si aparecen nuevas estaciones
				checkForNewStations();
					
				// miramos si tenemos nuevos datos en nuestras estaciones
				for (Station station : sService.getStations() ){ 
					LatestReading thread = applicationContext.getBean(LatestReading.class);
					thread.setStationIDx(station.getIdx());
					taskExecutor.execute(thread);
				}
				sleep(Keys.SLEEP_SECONDS*1000);	
			}
			catch (Exception e) {e.printStackTrace();}
		}
	}
	
	
	private void checkForNewStations(){
		try {
			JSONObject query = new JSONObject( reader.getStationsInAsturias() );
			if (query != null){
				JSONArray stations = query.getJSONArray("data");
				
				for (int i=0; i<stations.length(); i++){
					JSONObject data = stations.getJSONObject(i);
					Long idx = data.getLong("uid");
					
					if (sService.getStationByIDx(idx) == null){
						JSONObject stationJSON = data.getJSONObject("station");
						JSONArray geo = stationJSON.getJSONArray("geo");
						Double latitude = geo.getDouble(0);
						
						if (latitude != 0){
							Station station = new Station();
							station.setCity(cService.getCityById(0));
							station.setName(stationJSON.getString("name").split(",")[0]);
							station.setLatitude(latitude);
							station.setLongitude(geo.getDouble(1));
							station.setIdx(idx);
							
							sService.insert(station);
						}
					}
				}
			}
		} 
		catch (JSONException e) {e.printStackTrace();}
	}
}
