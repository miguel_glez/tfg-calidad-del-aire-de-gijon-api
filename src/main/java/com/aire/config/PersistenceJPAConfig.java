package com.aire.config;



import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

/*import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;*/
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.aire.utils.DatabaseProperties;

@Configuration
@EnableTransactionManagement
public class PersistenceJPAConfig{
 
   @Bean
   public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
      em.setDataSource(dataSource());
      em.setPackagesToScan(new String[] { "com.aire.domain" });
 
      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
      em.setJpaVendorAdapter(vendorAdapter);
      em.setJpaProperties(additionalProperties());
 
      return em;
   }
 
   @Bean
   public DataSource dataSource(){
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setUrl("jdbc:mysql://"+DatabaseProperties.get("host")+"/"+DatabaseProperties.get("scheme"));
      dataSource.setDriverClassName("com.mysql.jdbc.Driver");
      dataSource.setUsername( DatabaseProperties.get("username") );
      dataSource.setPassword( DatabaseProperties.get("password") );
      return dataSource;
   }
 
   @Bean
   public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
      JpaTransactionManager transactionManager = new JpaTransactionManager();
      transactionManager.setEntityManagerFactory(emf);
 
      return transactionManager;
   }
 
   @Bean
   public PersistenceExceptionTranslationPostProcessor exceptionTranslation(){
      return new PersistenceExceptionTranslationPostProcessor();
   }
 
   Properties additionalProperties() {
      Properties properties = new Properties();
      properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
      properties.setProperty("hibernate.hbm2ddl.auto", "update");
      properties.setProperty("hibernate.connection.url", "jdbc:mysql://"+DatabaseProperties.get("host")+"/"+DatabaseProperties.get("scheme") );
      return properties;
   }
   
   /*
   // tomcat extra config for https
   
   @Bean
   public EmbeddedServletContainerFactory servletContainer() {
	   TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory() {
		   @Override
		   protected void postProcessContext(Context context) {
			   SecurityConstraint securityConstraint = new SecurityConstraint();
			   securityConstraint.setUserConstraint("CONFIDENTIAL");
			   SecurityCollection collection = new SecurityCollection();
			   collection.addPattern("/*");
			   securityConstraint.addCollection(collection);
			   context.addConstraint(securityConstraint);
		   }
	   };

	   tomcat.addAdditionalTomcatConnectors(initiateHttpConnector());
	   return tomcat;
   }

   private Connector initiateHttpConnector() {
	   Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
	   connector.setScheme("http");
	   connector.setPort(8080);
	   connector.setSecure(false);
	   connector.setRedirectPort(8443);

	   return connector;
   }*/

   /*@Bean
   public EmbeddedServletContainerCustomizer containerCustomizer() {
	   return new EmbeddedServletContainerCustomizer() {
		   @Override
		   public void customize(ConfigurableEmbeddedServletContainer container) {
			   if (container instanceof TomcatEmbeddedServletContainerFactory) {
				   TomcatEmbeddedServletContainerFactory containerFactory = (TomcatEmbeddedServletContainerFactory) container;
				   Connector connector = new Connector(TomcatEmbeddedServletContainerFactory.DEFAULT_PROTOCOL);
				   connector.setPort(8443);
				   containerFactory.addAdditionalTomcatConnectors(connector);
			   }
		   }
	   };
   }*/
   
}
