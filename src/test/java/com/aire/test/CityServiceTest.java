package com.aire.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;

import com.aire.domain.City;
import com.aire.domain.Station;
import com.aire.services.CityService;

public class CityServiceTest extends AireTest {
	@Autowired
	CityService service;
	
	// CS-001
	@Test
	public void whenGetAllCities_returnCities(){
		// tenemos
		insertCity();
		insertCity("Oviedo");
		
		// probamos
		List<City> cities = service.getAllCities();
		
		// comprobamos
		assertThat( cities.size() ).isEqualTo( 2 );
		assertThat( cities.get(0).getName() ).isEqualTo("Gijón"); 
	}
	
	// CS-002
	@Test
	public void whenGetAverageAirQuality_returnDouble(){
		// tenemos
		City mieres = insertCity("Mieres");
		Station station = insertStation(mieres);
		insertReadings(station, 200);
		
		// probamos
		double aiq = service.getAirQuality(mieres);
		
		// comprobamos
		assertThat( aiq ).isEqualTo( 5.0 ); // todas las lecturas son creadas con 5 aiq
	}
	
	// CS-003
	@Test
	public void whenGetAverageTemperature_returnDouble(){
		// tenemos
		City mieres = insertCity("Mieres");
		Station station = insertStation(mieres);
		insertReadings(station, 200);
		
		// probamos
		double t = service.getTemperature(mieres);
		
		// comprobamos
		assertThat( t ).isEqualTo( 10.0 ); // todas las lecturas son creadas con 10 grados
	}
	
	// CS-004
	@Test
	public void whenGetCityById_returnCity(){
		// tenemos
		City langreo = insertCity("Langreo");
		Long id = langreo.getCityID();
		
		// probamos
		City found = service.getCityById(id);
		
		// comprobamos
		assertThat( found.getCityID() ).isEqualTo( id ); 
	}
	
	// CS-005
	@Test
	public void whenGetCityByWrongId_returnNull(){
		// tenemos
		insertCity("Langreo");
		
		// probamos
		City found = service.getCityById(0);
		
		// comprobamos
		assertThat( found ).isNull();
	}
	
	// CS-006
	@Test
	public void whenGetCityByName_returnCity(){
		// tenemos
		String name = "Llanes";
		insertCity(name);
		
		// probamos
		City found = service.getCityByName(name);
		
		// comprobamos
		assertThat( found.getName() ).isEqualTo( name ); 
	}
}
