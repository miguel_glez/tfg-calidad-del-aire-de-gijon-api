package com.aire.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aire.domain.City;
import com.aire.domain.Station;
import com.aire.services.CityService;
import com.aire.services.ReadingService;
import com.aire.services.StationService;
import com.aire.utils.JSONUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@RestController
public class CityController {
	@Autowired
	CityService cService;
	
	@Autowired
	StationService sService;
	
	@Autowired
	ReadingService rService;
	
	@RequestMapping(value = "/city/stations", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getStations(@RequestParam(value="id", defaultValue="") Long cityID) {
		City city = cService.getCityById(cityID);
		
			if (city != null){
				JsonObject cityJSON = new JsonObject();
				cityJSON.addProperty("status", true);
				cityJSON.addProperty("id", city.getCityID());
				cityJSON.addProperty("name", city.getName());
				cityJSON.addProperty("temperature", cService.getTemperature(city));
				cityJSON.addProperty("aqi", cService.getAirQuality(city));
				
				JsonArray stationsJSON = new JsonArray();			
				for (Station station : sService.getCityStations(city)){	
					JsonObject stationJSON = JSONUtils.createStationJSON(station);
					stationJSON.add("latestReading", JSONUtils.createReadingJSON(rService.getLatestReading(station)));
					stationsJSON.add(stationJSON);
				}
				cityJSON.add("stations", stationsJSON);
				
				return new ResponseEntity<String>(cityJSON.toString(), HttpStatus.OK);
			}
		

		return new ResponseEntity<String>("{\"status\": false}", HttpStatus.BAD_REQUEST);
    }
}
