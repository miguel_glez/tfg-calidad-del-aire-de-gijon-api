package com.aire.utils;

import java.time.OffsetDateTime;

import org.json.JSONException;
import org.json.JSONObject;

import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JSONUtils {
	private JSONUtils(){}
	
	public static Double getValue(JsonObject json){
		try {
			return json.get("v").getAsDouble();
		} 
		catch (Exception e) { return 0.0;}
	}
	
	public static Double getValue(JSONObject json){
		try {
			return json.getDouble("v");
		} 
		catch (Exception e) { return 0.0;}
	}
	
	public static JsonObject createStationJSON(Station station){
		JsonObject stationJSON = new JsonObject();
		stationJSON.addProperty("id", station.getStationID());
		
		String name = station.getName();
		if (name.equals("")) name = station.getCity().getName();
		stationJSON.addProperty("name", name);
		
		stationJSON.addProperty("longitude", station.getLongitude());
		stationJSON.addProperty("latitude", station.getLatitude());
		stationJSON.addProperty("cityID", station.getCity().getCityID());
		
		return stationJSON;
	}
	
	public static JsonObject createReadingJSON(Reading reading){
		try {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			JSONObject object = new JSONObject( gson.toJson(reading) );
			object.put("readingID", reading.getReadingID());
			object.put("date", reading.getDate().toInstant(OffsetDateTime.now().getOffset()).toEpochMilli());
			//object.put("timestamp", Timestamp.valueOf(reading.getDate()));
			
			JsonParser jsonParser = new JsonParser();
		    JsonObject gsonObject = (JsonObject)jsonParser.parse(object.toString());
		    
		    return gsonObject;
		} 
		catch (JSONException e) {}
		return null;
	}
	
}
