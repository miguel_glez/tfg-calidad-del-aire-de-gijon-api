package com.aire.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.aire.repositories.ReadingRepository;

@Service
public class ReadingService {
	@Autowired
	ReadingRepository repository;
	
	public void insert(Reading reading){
		// A veces el reading que nos llega desde la api no es correcto
		// y marca como más contaminante algo que no lo es
		HashMap<String, Double> values = new HashMap<>();
		values.put("co", reading.getCarbonMonoxide());
		values.put("no2", reading.getNitrogenDioxide());
		values.put("o3", reading.getOzone());
		values.put("so2", reading.getSulfurDioxide());
		values.put("pm25", reading.getPm25());
		values.put("pm10", reading.getPm10());
		
		String maxPolluter = "";
		Double aqi = 0.0;
		for (Entry<String, Double> entry: values.entrySet()){
			if (entry.getValue() > aqi){
				maxPolluter = entry.getKey();
				aqi = entry.getValue();
			}
		}
		reading.setAirQuality(aqi);
		reading.setDominentPolluter(maxPolluter);
		
		repository.save(reading);
	}
	
	public void delete(Reading reading){
		repository.delete(reading);
	}
	
	public Reading getLatestReading(Station station){
		return repository.findFirstByStationOrderByReadingIDDesc(station);
	}
		
	public Reading getReadingById(Long id){
		return repository.findOne(id);
	}
	
	public Reading getReadingById(int id){
		return getReadingById( new Long(id) );
	}
	
	public List<Reading> getLastWeekReadings(Station station){
		return getReadingsBetweenDates(station, LocalDateTime.now().minusHours(24*7), LocalDateTime.now());
	}
	
	public List<Reading> getLastDayReadings(Station station){
		return getReadingsBetweenDates(station, LocalDateTime.now().minusHours(24), LocalDateTime.now());
	}
	
	public boolean lastHourFound(Station station){
		return getReadingsBetweenDates(station, LocalDateTime.now().minusHours(1), LocalDateTime.now()).size() > 0;
	}
	
	public List<Reading> getReadingsBetweenDates(Station station, LocalDateTime start, LocalDateTime end){
		ArrayList<Reading> readings = new ArrayList<Reading>();
		
		for (Reading reading: repository.findTop168ByStationOrderByReadingIDDesc(station) ){
			if (reading.getDate().isAfter(start) && reading.getDate().isBefore(end) ){
				readings.add( reading );
			}
		}
		return readings;
	}
}