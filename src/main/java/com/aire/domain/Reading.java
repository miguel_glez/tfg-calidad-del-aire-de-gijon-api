package com.aire.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.aire.utils.LocalDateTimeAttributeConverter;
import com.google.gson.annotations.Expose;

@Entity
@Table(name = "station_data")
public class Reading implements Serializable{

	private static final long serialVersionUID = -195670049657748504L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long readingID;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stationID")
	private Station station;
	
	@Convert(converter = LocalDateTimeAttributeConverter.class)
	private LocalDateTime date;
	
	@Expose
	private String dominentPolluter;	// dominentpol
	
	@Expose
	private Double airQuality;			// aqi
	
	@Expose
	private Double carbonMonoxide; 		// co
	
	@Expose
	private Double humidity;			// h
	
	@Expose
	private Double nitrogenDioxide;		// no2
	
	@Expose
	private Double ozone; 	 			// o3
	
	@Expose
	private Double pressure; 			// p
	
	@Expose
	private Double pm10;				// pm10
	
	@Expose
	private Double pm25;				// pm25
	
	@Expose
	private Double sulfurDioxide;		// so2
	
	@Expose
	private Double temperature; 		// t
	
	public Reading(Station station, LocalDateTime date, String dominentPolluter, Double airQuality, Double carbonMonoxide,
			Double humidity, Double nitrogenDioxide, Double ozone, Double pressure, Double pm10, Double pm25, Double sulfurDioxide,
			Double temperature) {
		this.station = station;
		this.date = date;
		this.dominentPolluter = dominentPolluter;
		this.airQuality = airQuality;
		this.carbonMonoxide = carbonMonoxide;
		this.humidity = humidity;
		this.nitrogenDioxide = nitrogenDioxide;
		this.ozone = ozone;
		this.pressure = pressure;
		this.pm10 = pm10;
		this.pm25 = pm25;
		this.sulfurDioxide = sulfurDioxide;
		this.temperature = temperature;
	}

	public Reading() {
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public String getDominentPolluter() {
		return dominentPolluter;
	}

	public void setDominentPolluter(String dominentPolluter) {
		this.dominentPolluter = dominentPolluter;
	}

	public Double getAirQuality() {
		return airQuality;
	}

	public void setAirQuality(Double airQuality) {
		this.airQuality = airQuality;
	}

	public Double getCarbonMonoxide() {
		return carbonMonoxide;
	}

	public void setCarbonMonoxide(Double carbonMonoxide) {
		this.carbonMonoxide = carbonMonoxide;
	}

	public Double getHumidity() {
		return humidity;
	}

	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}

	public Double getNitrogenDioxide() {
		return nitrogenDioxide;
	}

	public void setNitrogenDioxide(Double nitrogenDioxide) {
		this.nitrogenDioxide = nitrogenDioxide;
	}

	public Double getOzone() {
		return ozone;
	}

	public void setOzone(Double ozone) {
		this.ozone = ozone;
	}

	public Double getPressure() {
		return pressure;
	}

	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}

	public Double getPm10() {
		return pm10;
	}

	public void setPm10(Double pm10) {
		this.pm10 = pm10;
	}

	public Double getPm25() {
		return pm25;
	}

	public void setPm25(Double pm25) {
		this.pm25 = pm25;
	}

	public Double getSulfurDioxide() {
		return sulfurDioxide;
	}

	public void setSulfurDioxide(Double sulfurDioxide) {
		this.sulfurDioxide = sulfurDioxide;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

	public Long getReadingID() {
		return readingID;
	}

	public Station getStation() {
		return station;
	}

	public void setStation(Station station) {
		this.station = station;
	}
	
}
