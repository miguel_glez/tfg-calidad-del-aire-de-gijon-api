package com.aire.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.aire.domain.City;
import com.aire.domain.Reading;
import com.aire.domain.Station;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest(classes = { TestConfig.class } )
@ComponentScan(basePackages = { "com.aire.services", "com.aire.repositories"})
@DataJpaTest

abstract public class AireTest {
	
	@Autowired
    protected TestEntityManager entityManager;	
	
	public City insertCity(){
		return insertCity("Gijón");
	}
	
	public City insertCity(String name) {
		City xixon = new City(name);
		entityManager.persist(xixon);
		entityManager.flush();
		
		return xixon;
    }
	
	public Station insertStation(City city){
		Station castilla = new Station(new Long(6702), "Avda Castilla", 43.5369319, -5.6473465, city);
		entityManager.persist(castilla);
		entityManager.flush();

		return castilla;
	}
	
	public Reading insertReading(Station station, LocalDateTime date){
		Reading reading = new Reading(station, date, "o3", 5.0, null, null, null, null, null, null, null, null, 10.0);
		entityManager.persist(reading);
		entityManager.flush();
		
		return reading;
	}
	
	public void insertReadings(Station station, int n){
		for(int i=1; i<=n; i++){
			Reading reading = new Reading(station, LocalDateTime.now().minusHours(200 - (1*i)), "o3", 5.0, null, null, null, null, null, null, null, null, 10.0);
			entityManager.persist(reading);
		}
		entityManager.flush();
	}
}
