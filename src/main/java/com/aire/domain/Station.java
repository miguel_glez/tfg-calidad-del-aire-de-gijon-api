package com.aire.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "station")
public class Station implements Serializable{
	private static final long serialVersionUID = 3120186254077866251L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long stationID;
	private Long idx;
	private String name;
	
	private Double latitude;
	private Double longitude;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cityID")
	private City city;
	

	public Station(Long idx, String name, Double latitude, Double longitude, City city) {
		super();
		this.idx = idx;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.city = city;
	}

	public Station() {
		super();
	}

	public Long getIdx() {
		return idx;
	}

	public void setIdx(Long idx) {
		this.idx = idx;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Long getStationID() {
		return stationID;
	}
		
}
