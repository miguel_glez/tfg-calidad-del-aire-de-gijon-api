package com.aire.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan({"com.aire.controllers", "com.aire.fetch", "com.aire.services"})
@EntityScan("com.aire.domain")
@EnableJpaRepositories("com.aire.repositories")
public class AppConfig{
}
