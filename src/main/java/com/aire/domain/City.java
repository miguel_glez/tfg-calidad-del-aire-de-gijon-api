package com.aire.domain;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city")
public class City implements Serializable {

	private static final long serialVersionUID = 6704604150910506698L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long cityID;
	private String name;
	
	public City(String name) {
		super();
		this.name = name;
	}
	
	public City() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getCityID() {
		return cityID;
	}
	
}
