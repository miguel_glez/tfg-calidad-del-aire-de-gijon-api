package com.aire.fetch;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.aire.utils.Keys;

public class StationReader {
	private final RestTemplate restTemplate;

    public StationReader(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getLatestReading(Long stationIDx) {
    	try{
	    	ResponseEntity<String> response = this.restTemplate.getForEntity("https://api.waqi.info/feed/@"+stationIDx.toString()+"/?token="+Keys.AQICN_TOKEN, String.class);
	    	
	    	if (response.getStatusCode().equals(HttpStatus.OK)){
	    		return response.getBody();
	    	}
    	}
    	catch (Exception e){
    		System.out.println("Failed to fetch data from AIQ: station/"+stationIDx.toString());
    	}
        return null;
    }
    
    public String getStationsInAsturias(){
    	try{
	    	ResponseEntity<String> response = this.restTemplate.getForEntity("https://api.waqi.info/search/?token="+Keys.AQICN_TOKEN+"&keyword=asturias", String.class);
	    
	    	if (response.getStatusCode().equals(HttpStatus.OK)){
	    		return response.getBody();
	    	}
    	}
    	catch(Exception e){
    		System.out.println("Failed to fetch data from AIQ: stations/keyword=asturias");
    	}
        return null;
    }
}
