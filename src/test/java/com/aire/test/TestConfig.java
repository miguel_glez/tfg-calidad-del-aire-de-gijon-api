package com.aire.test;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@Configuration
@ComponentScan({"com.aire.controllers", "com.aire.fetch", "com.aire.services"})
@EntityScan("com.aire.domain")
@EnableJpaRepositories("com.aire.repositories")

public class TestConfig{
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	
}
