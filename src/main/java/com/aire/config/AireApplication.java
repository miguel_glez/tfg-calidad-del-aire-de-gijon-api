package com.aire.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.web.client.RestTemplate;

import com.aire.fetch.FetchDataThread;

@EntityScan(
	basePackageClasses = { AireApplication.class, Jsr310JpaConverters.class }
)

@SpringBootApplication
public class AireApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(AireApplication.class, args);
		
		@SuppressWarnings("resource") // no se debe cerrar -> te cargas la aplicacion
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AireApplication.class);
		
		// lanzar el hilo que espera por los datos
		FetchDataThread fetcher = (FetchDataThread) ctx.getBean("fetchDataThread");
		fetcher.start();
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
}
