package com.aire.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aire.domain.City;
import com.aire.domain.Station;
import com.aire.services.StationService;

public class StationServiceTest extends AireTest{
	@Autowired
	StationService service;
	
	// SS-001
	@Test
	public void whenGetStationById_returnStation(){
		// tenemos
		Station station = insertStation( insertCity("Langreo") );
		Long id = station.getStationID();
				
		// probamos
		Station found = service.getStationByID( id );
		
		// comprobamos
		assertThat( found.getStationID() ).isEqualTo( id ); 
	}
	
	// SS-002
	@Test
	public void whenGetStationByWrongId_returnNull(){
		// tenemos
		insertStation( insertCity("Langreo") );
				
		// probamos
		Station found = service.getStationByID( (long) 124 );
		
		// comprobamos
		assertThat( found ).isNull(); 
	}
	
	// SS-003
	@Test
	public void whenGetStationByIdx_returnStation(){
		// tenemos
		Station station = insertStation( insertCity("Langreo") );
		Long idx = station.getIdx();
				
		// probamos
		Station found = service.getStationByIDx( idx );
		
		// comprobamos
		assertThat( found.getIdx() ).isEqualTo( idx ); 
	}
	
	// SS-004
	@Test
	public void whenGetStationByWrongIdx_returnNull(){
		// tenemos
		insertStation( insertCity("Langreo") );
				
		// probamos
		Station found = service.getStationByIDx( (long) 124 );
		
		// comprobamos
		assertThat( found ).isNull(); 
	}
	
	// SS-005
	@Test
	public void whenGetStations_returnStations(){
		// tenemos
		City xxn = insertCity();
		insertStation(xxn);
		insertStation(xxn);
		insertStation(xxn);
		City ovi = insertCity("Oviedo");
		insertStation(ovi);
		
		// probamos
		List<Station> found = service.getStations();
		
		// comprobamos
		assertThat( found.size() ).isEqualTo(4);
	}
	
	// SS-006
	@Test
	public void whenGetCityStations_returnStations(){
		// tenemos
		City xxn = insertCity();
		insertStation(xxn);
		insertStation(xxn);
		insertStation(xxn);
		City ovi = insertCity("Oviedo");
		insertStation(ovi);
		
		// probamos
		List<Station> found = service.getCityStations(xxn);
		
		// comprobamos
		assertThat( found.size() ).isEqualTo(3);
	}
}
