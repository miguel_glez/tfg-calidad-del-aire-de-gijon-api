package com.aire.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.aire.domain.City;
import com.aire.domain.Station;

@Repository
public interface StationRepository extends CrudRepository<Station, Long> {
	Station findByName(String name);
	Station findByIdx(Long idx);
	List<Station> findByCity(City city);
}