package com.aire.fetch;

import java.time.LocalDateTime;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.aire.services.CityService;
import com.aire.services.ReadingService;
import com.aire.services.StationService;
import com.aire.utils.JSONUtils;
import com.aire.utils.LocalDateTimeConverter;

@Component
@Scope("prototype")
public class LatestReading implements Runnable {
	@Autowired
	ReadingService rService;
	@Autowired
	StationService sService;
	@Autowired
	CityService cService;
	
	Long stationIDx;
	private StationReader reader;
	
	public LatestReading(){
		this.reader = new StationReader(new RestTemplateBuilder());
	}
	
	public void setStationIDx( Long stationIDx ){
		this.stationIDx = stationIDx;
	}
	
	public void run(){
		getLatestReadingFromStationID(true);
	}
	
	private void getLatestReadingFromStationID(boolean insistir){
		Station station = sService.getStationByIDx(this.stationIDx);
		try {
			Reading lastReadingSaved = rService.getLatestReading(station);	
		
			String readingStr = reader.getLatestReading( this.stationIDx );
			if ( readingStr != null ) {
				Reading latest = new Reading();	
			
				JSONObject json = new JSONObject( readingStr );
				if (json.optString("status").equals("ok")){
					JSONObject data = json.getJSONObject( "data" ) ;
					JSONObject timeJson = data.getJSONObject( "time" ) ;
					JSONObject iaqi = data.getJSONObject("iaqi" );
					
					LocalDateTime rTime = LocalDateTimeConverter.millsToLocalDateTime(timeJson.getString("s"));
					if (lastReadingSaved != null){
						if (lastReadingSaved.getDate().isEqual(rTime)) {
							System.out.println("["+LocalDateTime.now().toString() + "] Los datos de la estacion: "+station.getStationID()+" ya estaban actualizados.");
							return; // si nos coinciden los tiempos del nuevo y viejo -> ya tenemos esa info
						}
					}
					
					// set the data
					latest.setDate(rTime);
					latest.setStation(station);
					latest.setAirQuality(data.getDouble("aqi"));
					latest.setDominentPolluter(data.getString("dominentpol"));
					
					// values
					latest.setCarbonMonoxide(JSONUtils.getValue( iaqi.optJSONObject("co") ));
					latest.setHumidity(JSONUtils.getValue( iaqi.optJSONObject("h") ));
					latest.setNitrogenDioxide(JSONUtils.getValue( iaqi.optJSONObject("no2") ));
					latest.setOzone(JSONUtils.getValue( iaqi.optJSONObject("o3") ));
					latest.setPressure(JSONUtils.getValue( iaqi.optJSONObject("p") ));
					latest.setPm10(JSONUtils.getValue( iaqi.optJSONObject("pm10") ));
					latest.setPm25(JSONUtils.getValue( iaqi.optJSONObject("pm25") ));
					latest.setSulfurDioxide(JSONUtils.getValue( iaqi.optJSONObject("so2") ));
					latest.setTemperature(JSONUtils.getValue( iaqi.optJSONObject("t") ));
					
					System.out.println("["+LocalDateTime.now().toString() + "] Leidos datos de estacion: "+station.getStationID());
					
					rService.insert(latest);
					return;
				}
				System.out.println("["+LocalDateTime.now().toString() + "] Los datos de la estacion: "+station.getStationID()+" no pudieron ser descargados. Status != ok. Insistimos? "+String.valueOf(insistir));
				if (insistir)
					getLatestReadingFromStationID(false);
				return;
			}
			return;
		} 
		catch (JSONException e) {
			System.out.println("["+LocalDateTime.now().toString() + "] Los datos de la estacion: "+station.getStationID()+" no pudieron ser descargados.");
			System.out.println(e.getStackTrace()[0].toString() + e.getStackTrace()[1].toString());
		}
		return;
	}
	
}
