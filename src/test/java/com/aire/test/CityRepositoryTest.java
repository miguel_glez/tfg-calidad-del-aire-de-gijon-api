package com.aire.test;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.assertThat;

import com.aire.domain.City;
import com.aire.repositories.CityRepository;

public class CityRepositoryTest extends AireTest {
	
	@Autowired
	private CityRepository repository;
	
	// CR-001
	@Test
	public void whenFindByName_thenReturnCity(){
		// tenemos
		City xixon = insertCity();
		
		// probamos
		City found = repository.findByName("Gijón");
		
		// comprobamos
		assertThat( found.getName() ).isEqualTo( xixon.getName() );
	}
	
	// CR-002
	@Test
	public void whenFindByWrongName_thenReturnNull(){
		// tenemos
		insertCity();
		
		// probamos
		City found = repository.findByName("albacete");
		
		// comprobamos
		assertThat( found ).isNull();
	}
}
