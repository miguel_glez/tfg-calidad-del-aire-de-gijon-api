package com.aire.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aire.domain.City;
import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.aire.repositories.CityRepository;

@Service
public class CityService {
	@Autowired
	CityRepository repository;
	
	@Autowired
	StationService sService;
	
	@Autowired
	ReadingService rService;
	
	public void insert(City city){
		repository.save(city);
	}
	
	public void delete(City city){
		repository.delete(city);
	}
	
	public City getCityByName(String name){
		return repository.findByName(name);
	}
	
	public City getCityById(Long id){
		return repository.findOne(id);
	}
	
	public City getCityById(int id){
		return getCityById( new Long(id) );
	}
	
	public List<City> getAllCities(){
		ArrayList<City> cities = new ArrayList<City>();
		for (City city: repository.findAll()){
			cities.add( city );
		}
		return cities;
	}
	
	public double getTemperature(City city){
		double temp = 0;
		List<Station> stations = sService.getCityStations(city);
		for (Station station : stations){
			Reading lastReading = rService.getLatestReading(station);
			temp = temp + lastReading.getTemperature();
		}
		return temp / stations.size();
	}
	
	public double getAirQuality(City city){
		double aiq = 0;
		for (Station station : sService.getCityStations(city)){
			Reading lastReading = rService.getLatestReading(station);
			if (lastReading.getAirQuality() > aiq) 
				aiq = lastReading.getAirQuality();
		}
		return aiq;
	}
}
