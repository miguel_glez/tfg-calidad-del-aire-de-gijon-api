package com.aire.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aire.domain.City;
import com.aire.domain.Station;
import com.aire.repositories.StationRepository;

@Service
public class StationService {
	@Autowired
	StationRepository repository;
	
	public void insert(Station station){
		repository.save(station);
	}
	
	public void delete(Station station){
		repository.delete(station);
	}
	
	public Station getStationByID(Long stationID){
		return repository.findOne(stationID);
	}

	public Station getStationByIDx(Long stationIDx) {
		return repository.findByIdx(stationIDx);
	}
	
	public List<Station> getCityStations(City city){
		List<Station> list = new ArrayList<Station>();
		for (Station station : repository.findByCity(city)) {
			list.add(station);
		}
		return list;
	}

	public List<Station> getStations() {
		List<Station> list = new ArrayList<Station>();
		for (Station station : repository.findAll()) {
			list.add(station);
		}
		return list;
	}
}
