package com.aire.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.aire.domain.City;
import com.aire.domain.Station;
import com.aire.repositories.StationRepository;

public class StationRepositoryTest extends AireTest{
	
	@Autowired
	private StationRepository repository;
		
	// SR-001
	@Test
	public void whenFindByName_thenReturnStation(){		
		// tenemos
		Station castilla = insertStation(insertCity());
		
		// probamos
		Station found = repository.findByName("Avda Castilla");
		
		// comprobamos
		assertThat( found.getName() ).isEqualTo( castilla.getName() );
	}
	
	// SR-002
	@Test
	public void whenFindByIdx_thenReturnStation(){		
		// tenemos
		Station castilla = insertStation(insertCity());
		Long idx = new Long(6702); // id q tiene la estacion insertada
		
		// probamos
		Station found = repository.findByIdx(idx);
		
		// comprobamos
		assertThat( found.getIdx() ).isEqualTo( castilla.getIdx() );
	}
	
	// SR-003
	@Test
	public void whenFindByCity_thenReturnStations(){		
		// tenemos
		City xixon = insertCity();
		Station castilla = insertStation(xixon);
		
		// probamos
		List<Station> found = repository.findByCity(xixon);
		
		// comprobamos
		assertThat( found.get(0).getName() ).isEqualTo( castilla.getName() );
	}
}
