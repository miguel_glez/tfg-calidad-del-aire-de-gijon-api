package com.aire.test;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.assertThat;

import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.aire.services.ReadingService;

public class ReadingServiceTest extends AireTest{
	@Autowired
	ReadingService service;
	
	// RS-001
	@Test
	public void whenGetReadingsBetweenDates_returnReadings(){
		// tenemos
		Station castilla = insertStation(insertCity());
		insertReadings(castilla, 200);
		
		// probamos
		List<Reading> found = service.getReadingsBetweenDates(castilla, LocalDateTime.now().minusHours(3).minusMinutes(5), LocalDateTime.now());
		
		// comprobamos
		assertThat( found.size() ).isEqualTo(4);
	}
	
	// RS-002
	@Test
	public void whenGetReadingsBetweenWrongDates_returnEmptyList(){
		// tenemos
		Station castilla = insertStation(insertCity());
		insertReadings(castilla, 200);
		
		// probamos
		List<Reading> found = service.getReadingsBetweenDates(castilla, LocalDateTime.now(), LocalDateTime.now().minusHours(3).minusMinutes(5));
		
		// comprobamos
		assertThat( found.size() ).isEqualTo(0);
	}
}
