package com.aire.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aire.domain.Reading;
import com.aire.domain.Station;
import com.aire.services.ReadingService;
import com.aire.services.StationService;
import com.aire.utils.JSONUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@RestController
public class StationController {
	@Autowired
	StationService sService;
	
	@Autowired
	ReadingService rService;
	
	@RequestMapping(value = "/station/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getStations() {
		JsonObject query = new JsonObject();
		JsonArray stationsJSON = new JsonArray();
		query.addProperty("status", true);
		for (Station station : sService.getStations()){			
			JsonObject stationJSON = JSONUtils.createStationJSON(station);
			stationJSON.add("latestReading", JSONUtils.createReadingJSON(rService.getLatestReading(station)));
			stationsJSON.add( stationJSON );
		}
		query.add("stations", stationsJSON);

		return new ResponseEntity<String>(query.toString(), HttpStatus.OK);
    }
	
	@RequestMapping(value = "/station/get", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getStation(@RequestParam(value="id", defaultValue="") Long stationID) {
		Station station = sService.getStationByID(stationID);

		if (station != null){
			JsonObject stationJSON = new JsonObject();
			stationJSON.addProperty("status", true);
			
			stationJSON.add("station", JSONUtils.createStationJSON(station));
			stationJSON.add("latestReading", JSONUtils.createReadingJSON(rService.getLatestReading(station)));

			return new ResponseEntity<String>(stationJSON.toString(), HttpStatus.OK);
		}
		return new ResponseEntity<String>("{\"status\": false}", HttpStatus.BAD_REQUEST);
    }
	
	@RequestMapping(value = "/station/getLastDay", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getStationLastDay(@RequestParam(value="id", defaultValue="") Long stationID) {
		Station station = sService.getStationByID(stationID);

		if (station != null){
			JsonObject stationJSON = new JsonObject();
			stationJSON.addProperty("status", true);
			stationJSON.addProperty("name", station.getName());
			stationJSON.addProperty("stationID", station.getStationID());

			JsonArray dayJSON = new JsonArray();
			for (Reading reading : rService.getLastDayReadings(station)){
				dayJSON.add( JSONUtils.createReadingJSON(reading) );
			}
			stationJSON.add("data", dayJSON);

			return new ResponseEntity<String>(stationJSON.toString(), HttpStatus.OK);
		}

		return new ResponseEntity<String>("{\"status\": false}", HttpStatus.BAD_REQUEST);
    }
	
	@RequestMapping(value = "/station/getLastWeek", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getStationLastWeek(@RequestParam(value="id", defaultValue="") Long stationID) {
		Station station = sService.getStationByID(stationID);

		if (station != null){
			JsonObject stationJSON = new JsonObject();
			stationJSON.addProperty("status", true);
			stationJSON.addProperty("name", station.getName());
			stationJSON.addProperty("stationID", station.getStationID());

			JsonArray weekJSON = new JsonArray();
			for (Reading reading : rService.getLastWeekReadings(station)){
				weekJSON.add( JSONUtils.createReadingJSON(reading) );
			}
			stationJSON.add("data", weekJSON);

			return new ResponseEntity<String>(stationJSON.toString(), HttpStatus.OK);
		}
		
		
		return new ResponseEntity<String>("{\"status\": false}", HttpStatus.BAD_REQUEST);
    }
}
